/**
 * Created by Igor on 2017-04-15.
 */
(function(){

    var dude;
    var bunny;
    var platforms;
    var ground;
    var cursors;
    var carrotHiden = true;
    var bunnyAnimation = {};


    var game = new Phaser.Game(800, 600, Phaser.AUTO, '', { preload: preload, create: create, update: update, render:render });

    function preload() {

        game.load.image('sky', 'img/assets/sky.png');
        game.load.image('ground', 'img/assets/platform.png');
        game.load.image('star', 'img/assets/star.png');
        game.load.spritesheet('dude', 'img/assets/dude.png', 32, 48);
        game.load.atlasJSONHash('bunny', 'img/bunny-sprite/sprites.png', 'img/bunny-sprite/sprites.json');

    }

    function create() {
        //  We're going to be using physics, so enable the Arcade Physics system
        game.physics.startSystem(Phaser.Physics.ARCADE);

        //  A simple background for our game
        game.add.sprite(0, 0, 'sky');

        //  The platforms group contains the ground and the 2 ledges we can jump on
        platforms = game.add.group();

        //  We will enable physics for any object that is created in this group
        platforms.enableBody = true;

        // Here we create the ground.
        ground = platforms.create(0, game.world.height - 64, 'ground');

        //  Scale it to fit the width of the game (the original sprite is 400x32 in size)
        ground.scale.setTo(2, 2);

        //  This stops it from falling away when you jump on it
        ground.body.immovable = true;

        //  Now let's create two ledges
        var ledge = platforms.create(400, 400, 'ground');

        ledge.body.immovable = true;

        /*ledge = platforms.create(-150, 250, 'ground');

        ledge.body.immovable = true;*/


        // createDude();
        createBunny();


        //  Our controls.
        cursors = game.input.keyboard.createCursorKeys();
    }

    function update() {
        // updateDude();
        updateBunny();
    }

    function render() {
        game.debug.bodyInfo(bunny, 32, 32);
        game.debug.spriteInfo(bunny, 32, 200);
    }

    function createDude(){
        // The dude and its settings
        // x, y, name
        dude = game.add.sprite(32, game.world.height - 150, 'dude');

        //  We need to enable physics on the dude
        game.physics.arcade.enable(dude);

        //  Player physics properties. Give the little guy a slight bounce.
        dude.body.bounce.y = 0.2;
        dude.body.gravity.y = 300;
        dude.body.collideWorldBounds = true;

        //  Our two animations, walking left and right.
        dude.animations.add('left', [0, 1, 2, 3], 10, true);
        dude.animations.add('right', [5, 6, 7, 8], 10, true);
    }    

    function updateDude(){
        //  Collide the dude and the stars with the platforms
        game.physics.arcade.collide(dude, platforms);

        //  Reset the players velocity (movement)
        dude.body.velocity.x = 0;

        if (cursors.left.isDown)
        {
            //  Move to the left
            dude.body.velocity.x = -150;

            dude.animations.play('left');
        }
        else if (cursors.right.isDown)
        {
            //  Move to the right
            dude.body.velocity.x = 150;

            dude.animations.play('right');
        }
        else
        {
            //  Stand still
            dude.animations.stop();

            dude.frame = 4;
        }

        //  Allow the dude to jump if they are touching the ground.
        if (cursors.up.isDown && dude.body.touching.down)
        {
            dude.body.velocity.y = -350;
        }
    }
    
    function createBunny() {
        //  This sprite is using a texture atlas for all of its animation data
        bunny = game.add.sprite(32, ground.body.position.y - 80, 'bunny');
        bunny.anchor.setTo(.5,.5);
        game.physics.arcade.enable(bunny);

        bunny.body.bounce.y = 0;
        bunny.body.gravity.y = 900;
        bunny.body.collideWorldBounds = true;

        //  Our two animations, walking left and right.
        bunnyAnimation.move = bunny.animations.add('move', [21,22,23,24,25,26,27,28], 15, true);
        bunnyAnimation.jump = bunny.animations.add('jump', [37, 29, 30, 32, 31 ], 20, false); // , 32, 33, 34

        bunnyAnimation.landing = bunny.animations.add('landing', [35, 36, 37, 38, 39], 5, false);
        bunnyAnimation.getCarrot = bunny.animations.add('get-carrot', [ 3, 4, 5, 6, 7 ], 20, false);
        bunnyAnimation.eat = bunny.animations.add('eat', [ 8, 9, 10 ], 5, true);
        bunnyAnimation.hideCarrot = bunny.animations.add('hide-carrot', [ 11, 12, 13, 14, 15 ], 40, false);

    }

    function scaleX( texture ){
        texture.scale.x *= -1;
    }

    function updateBunny(){
        //  Collide the bunny and the stars with the platforms
        game.physics.arcade.collide(bunny, platforms);

        //  Reset the players velocity (movement)
        bunny.body.velocity.x = 0;

        var touchDown = bunny.body.touching.down;
        var currentAnim = bunny.animations.currentAnim;

        if( ground.body.position.y < (bunny.body.position.y + bunny.body.height)){
            // bunny.body.position.y = ground.body.position.y - bunny.body.height;
            console.log('collapse');
        }

        if( currentAnim && ('jump' == currentAnim.name) ) {
           // bunny.animations.stop('jump');
        }

        /**
         * Moving left
         */
        if ( cursors.left.isDown )
        {
            bunny.scale.x = Math.abs(bunny.scale.x) * -1;
            if( !carrotHiden
                || ( (currentAnim == bunnyAnimation.hideCarrot)
                && !currentAnim.isFinished ) )
            {
                bunny.animations.play('hide-carrot');
                carrotHiden = true;
            } else {
                bunny.body.velocity.x = -200;
                if( touchDown ) {
                    bunny.animations.play('move');
                }
            }
        }
        /**
         * Moving right
         */
        else if (cursors.right.isDown )
        {
            bunny.scale.x = Math.abs(bunny.scale.x);
            if( !carrotHiden
                || ( (currentAnim == bunnyAnimation.hideCarrot)
                && !currentAnim.isFinished ) )
            {
                bunny.animations.play('hide-carrot');
                carrotHiden = true;
            } else {
                bunny.body.velocity.x = 200;
                if( touchDown ) {
                    bunny.animations.play('move');
                }
            }
        }
        /**
         * Stay
         */
        else if( touchDown )
        {
            if(carrotHiden) {
                bunny.animations.play('get-carrot');
                carrotHiden = false;
            } else if( currentAnim.isFinished ) {
                bunny.animations.play('eat');
            }
        }

        //  Allow the bunny to jump if they are touching the ground.
        if (cursors.up.isDown && touchDown)
        {
            carrotHiden = true;
            bunny.animations.play('jump');
            setTimeout(function () {
                bunny.body.velocity.y = -800;
            }, 100);

            console.log('jump');
        }
    }

})();
